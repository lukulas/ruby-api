<h1 align="center">
Ruby API
</h1>

<p align="center">api</p>

## Recursos
- ⚛️ **express** — Framework web
- ⚛️ **jsonwebtoken** — Autenticação nas rotas
- ⚛️ **swagger-ui-express** — Gerar documentação
- ⚛️ **typeorm** — (ORM) manipular o banco de dados
- ⚛️ **Yup** — Validação de paramatros 
## Iniciando o projeto localmente
- Instalando as dependências <br>
`yarn install` <br>
- Iniciando os containers <br>
`docker-compose up` <br>

#### Acessando a documentação da api(Swagger)
http://localhost:3333/docs

##### Gerar um token com essas credenciais e navegar pelas rotas
name: talles <br>
password: 123456
